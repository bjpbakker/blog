require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  setup do
    @comment = comments(:one)
  end

  test "should create comment" do
    p = Post.create( :title => 'Commentary Post', :body => 'Post to get a comment' )
    assert_difference('Comment.count') do
      post :create, post_id: p.id, comment: { body: @comment.body, post_id: @comment.post_id }
    end

    assert_redirected_to p
  end
end
